'use strict'

// variables
// son las obtenidas de las librerías de npm
var gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync'),
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    uglify = require('gulp-uglify'),
    usemin = require('gulp-usemin'),
    rev = require('gulp-rev'),
    cleanCss = require('gulp-clean-css'),
    flatmap = require('gulp-flatmap'),
    htmlmin = require('gulp-htmlmin');

const browserSync = require('browser-sync');

// tarea que genera un archivo css a partir de los scss
gulp.task('sass', function(){
    gulp.src('./css/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css'));
});

// tarea que monitorea de los scss de la carpeta css
// ejecución de la tarea sass cuando haya un cambio de algún scss
gulp.task('sass:watch', function(){
    gulp.watch('./css/*.scss', ['sass']);
});

// Tarea que busca cualquier cambio en los ficheros definidos y lo recarga en la ruta del server
// de tal forma que se sincroniza en el navegador
gulp.task('browser-sync', function(){
    var files = ['./*html', './css/*.css', './img/*.{png,jpg, gif}', './js/*.js']
    browserSync.init(files,{
        server: {
            baseDir: './'
        }
    });
});

// Tarea que ejecuta la browser-sync e inmediatamente la tarea que escucha los cambios
// Se ejecuta con el comando gulp
gulp.task('default', ['browser-sync'], function(){
    gulp.setMaxListeners('sass:watch');
});


gulp.task('clean', function(){
    return del(['dist']);
});

gulp.task('copyfonts', function(){
    gulp.src('./node_modules/open-iconic/font/fonts/*.{ttf,woff,eof,svg,eot,otf}*')
    .pipe(gulp.dest('./dist/fonts'));
});

gulp.task('imagemin', function(){
    return gulp.src('./images/*.{png, jpeg, gif}')
    .pipe(imagemin({optimizationLevel: 3, progressive: true, interlaced: true}))
    .pipe(gulp.dest('dist/images'));
});

gulp.task('usemin', function(){
    return gulp.src('./*.html')
        .pipe(flatmap(function(stream, file){
            return stream
                .pipe(usemin({
                    css: [rev()],
                    html: [function() {return htmlmin({collapseWhitespace: true})}],
                    js: [uglify(), rev()],
                    inlinejs: [uglify()],
                    inlinecss: [cleanCss(), 'concat']
                }));
        }))
        .pipe(gulp.dest('dist/'));
});

gulp.task('build', ['clean'], function(){
    gulp.start('copyfonts','imagemin', 'usemin');
});