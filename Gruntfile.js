module.exports = function (grunt){

    require('time-grunt')(grunt);
    require('jit-grunt')(grunt, {
        useminPrepare: 'grunt-usemin'
    });

    grunt.initConfig({
        
        sass: { // configurar sass
            dist: { // para que genere la version de distribucion
                files: [{
                    expand: true, // buscando en todos los archivos
                    cwd: 'css', // dentro de la carpeta css
                    src: ['*.scss'], // que tengan extension scss
                    dest: 'css', // los envie a una carpeta dist/css
                    ext: '.css' // y les ponga extension css
                }]
            }
        },

        watch: {
            files: ['css/*.scss'], // cuando se modifique un scss en la /css
            tasks: ['css'] // llamar a la tarea registrada como 'css'
        },

        browserSync: {
            dev: {
                bsFiles: { // archivos a sincronizar con el browser
                    src: [
                        'css/*.css',
                        '*.html',
                        'js/*.js'
                    ]
                },
                options: {
                    watchTask: true,
                    server: {
                        baseDir: './' // Directorio base para nuestro servidor
                    }
                }
            }
        },

        imagemin: {
            dynamic: {
                files: [{
                    expand: true, // buscar en todos los archivos
                    cwd: './', // dentro de la carpeta /
                    src: 'images/*.{png,gif,jpg,jpeg}', // con estas extensiones
                    dest: 'dist/' // y enviarlas a dist/ comprimidas
                }]
            }
        },

        copy: {
            html: {
                files: [{
                    expand: true, // copia todos los archivos
                    dot: true,
                    cwd: './',
                    src: ['*html'], // html
                    dest: 'dist' // y los deja en dist/
                }]
            },
            fonts: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: 'node_modules/open-iconic/font',
                    src: ['fonts/*.*'],
                    dest: 'dist'
                }]
            }
        }, 

        clean: {
            build: {
                src: ['dist/'] // eliminar carpeta dist/
            }
        }, 

        cssmin: {
            dist: {} // minificar css
        },

        uglify: {
            dist: {} // minificar y dificultar js
        },

        filerev: {
            options: { // opciones del código que se va a generar en los archivos para que no sean cacheables por el browser
                encoding: 'utf8',
                algorithm: 'md5',
                length: 20
            }, 
            release: {
                //filerev: release hashes md5 all assets (images, js and css) in dist directory
                files: [{
                    src: [
                        'dist/js/*.js',
                        'dist/css/*.css'
                    ]
                }]
            }
        }, 

        concat: {
            options: {
                separator: ';'
            }, 
            dist: {}
        },

        useminPrepare: { // coger los html y aplicarle cssmin y uglify y guardarlos en /dist
            foo: {
                dest: 'dist',
                src: ['index.html', 'about.html', 'precios.html', 'contacto.html', 'terminos.html']    
            },
            options: {
                flow: {
                    steps: {
                        css: ['cssmin'],
                        js: ['uglify']
                    },
                    post: {
                        css: [{
                            name: 'cssmin',
                            createConfig: function(context, block){
                                var generated = context.options.generated;
                                generated.options = {
                                    keepSpecialComments: 0,
                                    rebase: false
                                }
                            }
                        }]
                    }
                }
            }
        },

        usemin: {
            html: ['dist/index.html', 'dist/about.html', 'dist/precios.html', 'dist/contacto.html', 'terminos.html'],
            options: {
                assetsDir: ['dist', 'dist/css', 'dist/js']
            }
        }
    });

    grunt.registerTask('css', ['sass']); // registrar tarea
    grunt.registerTask('default', ['browserSync', 'watch']);
    grunt.registerTask('img:compress', ['imagemin']);
    grunt.registerTask('build', ['clean', 'copy', 'imagemin','useminPrepare','concat','cssmin','uglify','filerev', 'usemin'])
};